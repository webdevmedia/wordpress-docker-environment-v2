#!/bin/bash
SED=`which sed`
CURRENT_DIR=`dirname $0`
DOMAIN=$1
lOG_PATH=/var/www/app/logs/nginx

if [[ ! -d "$lOG_PATH" ]]; then
  mkdir -p $lOG_PATH
fi

echo "Creating hosting for:" $DOMAIN
$SED "s/{{DOMAIN}}/$DOMAIN/g" $CURRENT_DIR/site-conf.stub > /etc/nginx/sites-available/site.conf
