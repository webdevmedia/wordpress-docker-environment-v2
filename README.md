# Ready to use - WordPress Composer Docker Environment #

you are searching for a ready to use -WordPress Docker environment- which you have only start up with one command?   
## What is in this repository?


## How do I get a setup?
* run start command  `./.docker/shell/dockerStart.sh`


## How to fix the certificate error in the browser?

In MacOS Catalina I had it working by:

1. In finder in your prject folder right click on the cert file in `.docker/nginx/ssl/docker-ssl.crt` and add it to the System Keychain
2. Then open your Keychain Access window and search for your localhost cert.
3. Right click on the certificate and head to "Information". 
3. Set "Always Trust" for Secure Sockets Layer (SSL) as depicted below.

![Alt](https://bitbucket.org/webdevmedia/wordpress-docker-environment-v2/raw/9164381f81102ec3e638c9866a5385d5b5791e52/assets/certificate-fix.jpg)