#!/bin/bash
echo 'wordpress init'

WORDPRESS_CONFIG_DIST='.docker/wordPress/wp-config.php'
WORDPRESS_CONFIG='public/wp-config.php'
WORDPRESS_CONFIG_SAMPLE='public/wp-config-sample.php'

WORDPRESS_CONTENT_PATH='public/wp-content'
WORDPRESS_PLUGINS_PATH=${WORDPRESS_CONTENT_PATH}'/plugins'
WORDPRESS_THEMES_PATH=${WORDPRESS_CONTENT_PATH}'/themes'
WORDPRESS_PLUGIN_HELLO=${WORDPRESS_PLUGINS_PATH}'/hello.php'
WORDPRESS_THEME_TWENTYTWENTYONE='twentytwentyone'

WORDPRESS_ENVIORNMENT_SETTINGS='docker-enviornment-settings.php'

if [[ ! -d 'vendor' ]]; then
  composer install
fi

if [[ ! -d 'logs/wordpress' ]]; then
  mkdir -p 'logs/wordpress'
fi

if [[ ! -f ${WORDPRESS_CONTENT_PATH}'/mu-plugins/'${WORDPRESS_ENVIORNMENT_SETTINGS} ]]; then
  if [[ ! -d ${WORDPRESS_CONTENT_PATH}'/mu-plugins/' ]]; then
    mkdir ${WORDPRESS_CONTENT_PATH}'/mu-plugins/'
  fi

  cp .docker/wordPress/docker-enviornment-settings.php ${WORDPRESS_CONTENT_PATH}'/mu-plugins/'
fi

if [[ -f $WORDPRESS_CONFIG_SAMPLE ]]; then
  rm $WORDPRESS_CONFIG_SAMPLE
  rm $WORDPRESS_PLUGIN_HELLO

  mv ${WORDPRESS_THEMES_PATH}/${WORDPRESS_THEME_TWENTYTWENTYONE} ${WORDPRESS_THEMES_PATH}/_${WORDPRESS_THEME_TWENTYTWENTYONE}
  rm -rf ${WORDPRESS_THEMES_PATH}/twenty*
  mv ${WORDPRESS_THEMES_PATH}/_${WORDPRESS_THEME_TWENTYTWENTYONE} ${WORDPRESS_THEMES_PATH}/${WORDPRESS_THEME_TWENTYTWENTYONE}
fi

if [[ (-f "$WORDPRESS_CONFIG_DIST" && ! -f "$WORDPRESS_CONFIG") ]]; then
  ENV_FILE='.docker/.env'

  export $(grep -v '^#' $ENV_FILE | xargs)
  if [[ -f "$ENV_FILE" ]]; then
    SED=`which sed`

    echo "Creating wp-config for: https://" $HOST
    $SED "s/{{DB_USER}}/${DB_USER}/g;
          s/{{DB_PASSWORD}}/${DB_PASSWORD}/g;
          s/{{DB_NAME}}/${DB_NAME}/g;
          s/{{WORDPRESS_TABLE_PREFIX}}/${WORDPRESS_TABLE_PREFIX}/g;
          s/{{CONTAINER_NAME}}/${CONTAINER_NAME}/g;
          s/{{HOST}}/${HOST}/g" $WORDPRESS_CONFIG_DIST > $WORDPRESS_CONFIG
  fi
fi

if [[ ! -d ${WORDPRESS_CONTENT_PATH}'/uploads' ]]; then
  if [[ -f 'assets/uploads.zip' ]]; then
    unzip assets/uploads.zip -d ${WORDPRESS_CONTENT_PATH}

    if [[ -d ${WORDPRESS_CONTENT_PATH}'/__MACOSX' ]]; then
      rm  -rf ${WORDPRESS_CONTENT_PATH}/__MACOSX
    fi
  fi
fi

FALLBACK_IMAGE="fallback.png"
FALLBACK_IMAGE_SOURCE_PATH='.docker/assets/'${FALLBACK_IMAGE}
FALLBACK_IMAGE_TARGET_PATH='public/wp-content/'${FALLBACK_IMAGE}
if [[ ! -f $FALLBACK_IMAGE_TARGET_PATH && -f $FALLBACK_IMAGE_SOURCE_PATH ]]; then
  cp $FALLBACK_IMAGE_SOURCE_PATH $FALLBACK_IMAGE_TARGET_PATH
fi
